### shortcut
Sometimes, `xdg-desktop-icon` isn't enough.
___

#### Usage
`shortcut -e <command> <dir>` links an existing shortcut that you have, if there is one, to the folder you ask for.

`shortcut -n <command> <dir>` runs a small wizard to make a brand new shortcut.

`shortcut -r <shortcut>.desktop` simply deletes the existing

#### Installation/Uninstallation

- Make installation:

  `make install` should be enough.
  ```sh
  $ git clone https://gitlab.com/beethoven__/shortcut.git
  $ cd shortcut
  $ make install #installs it
  ```
  To remove it, do `make uninstall` in the shortcut directory.

- Manual installation:

  Just copy `shortcut` to a folder in your $PATH. For exmaple:
  ```sh
  $ git clone https://gitlab.com/beethoven__/shortcut.git
  $ cd shortcut
  $ sudo cp shortcut /usr/bin
  ```
  To remove it, do `rm /usr/bin/shortcut`
